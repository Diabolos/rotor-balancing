import serial
import binascii
import time
import struct

import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft, ifft, rfft

BAUDRATE = 115200                   # 115200 is default
PORT = '/dev/ttyUSB0'               # For windows 'COM7'

N_MEASURES = 500
FREQ = 100                          # 25 50 100 200 400
N_SEND_SET_FREQ = 20
FREQUENCIES = {                     # Codes for Chengtec ACC345 
  0: b'\x77\x05\x00\x0C\x00\x11',
  25: b'\x77\x05\x00\x0C\x04\x15',
  50: b'\x77\x05\x00\x0C\x05\x16',
  100: b'\x77\x05\x00\x0C\x06\x17',
  200: b'\x77\x05\x00\x0C\x07\x18',
  400: b'\x77\x05\x00\x0C\x08\x19',
}
HEADER_BLOCK = b'\x77\x10\x00\x6A'   # Chengtec ACC345 if scream mode enable cf. doc 2.6

ser = serial.Serial()

logDataT = np.empty(N_MEASURES)
logDataX = np.empty((N_MEASURES), dtype=np.float32)
logDataY = np.empty((N_MEASURES), dtype=np.float32)
logDataZ = np.empty((N_MEASURES), dtype=np.float32)
logDataR = np.empty((N_MEASURES), dtype=np.float32)
logDeltas = np.empty(N_MEASURES)

def initSerial():
    global ser
    ser.baudrate = BAUDRATE
    ser.port = PORT   
    ser.timeout = 10
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = 8
    ser.parity = serial.PARITY_NONE
    ser.rtscts = 0

def main():
    initSerial()
    global ser, tStart
    ser.open()
    print("ser = ",ser)
    tStart = time.time_ns()
    
    # Set baudrate
    #ser.write(b'\x77\x05\x00\x0B\x08\x18') # bd 230400
    
    #ser.read_until(b'\x77\x05\x00\x8B')
    #print("db set to 230400 : ",binascii.hexlify(ser.read(1)))

    #ser.write(b'\x77\x04\x00\x0A\x0E') # Save
       
    #ser.read_until(b'\x77\x05\x00\x8A')
    #print("Setting saved : ",binascii.hexlify(ser.read(1)))


    # set frequency
    for _ in range(N_SEND_SET_FREQ):
        ser.write(FREQUENCIES[FREQ])     
       
    ser.read_until(b'\x77\x05\x00\x8C')
    freqSetHex = ser.read(1)
    ser.read(1)                         # control byte not used here
    print(binascii.hexlify(freqSetHex))
    
    ser.write(b'\x77\x05\x00\x2A\x00\x2F')  # règle sur envoi des trois axes cf doc 2.1   block de 17 bytes

    print("Check for the biginning of data transfert: ",binascii.hexlify(ser.read_until(HEADER_BLOCK)))   # find the beginning of a block of data
    ser.read(13)                      # read the entire block to clean
    
   
    for i in range(0, N_MEASURES):
        
        tReadStart = time.time_ns()

        mHex = ser.read_until(HEADER_BLOCK)      # Header block before x,y and z
        xHex = ser.read(4)                          # 4 bytes for accX
        yHex = ser.read(4)                          # 4 bytes for accY
        zHex = ser.read(4)                          # 4 bytes for accZ
        mHex = ser.read(1)                          # 1 byte for control, not used here
      
        logDataT[i] = time.time_ns()-tStart         # nano sec
        logDataX[i] = struct.unpack("<f", xHex)[0]
        logDataY[i] = struct.unpack("<f", yHex)[0]
        logDataZ[i] = struct.unpack("<f", zHex)[0]
  
        logDeltas[i] = time.time_ns()-tReadStart

    fig, ax = plt.subplots(2, 1)
    ax[0].plot(logDataT, logDataX)
    ax[0].plot(logDataT, logDataY)
    ax[0].plot(logDataT, logDataZ)
    ax[0].set_xlabel('time in seconds')
    ax[0].set_ylabel('Accelaration g')
        
    # Fourier

    f_1 = abs(rfft(logDataY))   # la base en X doit être divisée par le nbre de seconds pour donner des Hz
    f_1x = []
    for i in range(0, len(f_1)):
        f_1x.append(i/((logDataT[N_MEASURES-1]-logDataT[0])/1000000000)) 

    ax[1].plot(f_1x, f_1)
    ax[1].set_xlabel('Frequency in Hz')
    ax[1].set_ylabel('Amplitude in ?')
    plt.show()
    
    deltaPP = []
    for i in range(1,len(logDataT)):

        deltaPP.append(logDataT[i]-logDataT[i-1])
    
    print("Frequency set to     : ",FREQ,"Hz status (00=OK): ",freqSetHex)
    print("Interval idéal       : ",1/FREQ,"sec")
    print('Temps de sript réel  : ',((time.time_ns()-tStart)/1000000000),"sec")
    print('')    
    print("*************************** DATA SET **************************************")
    print('')
    print('T de la 1ere mesure  : ',logDataT[0]/1000000000,"sec")
    print('T de la dernière mes.: ',logDataT[N_MEASURES-1]/1000000000,"sec")
    print("Interval moyen       : ",np.average(deltaPP)/1000000000,"|",np.average(deltaPP)/1000000000*FREQ*100,"% Interval idéal")
    print("Temps de mesure      : ",(logDataT[N_MEASURES-1]-logDataT[0])/1000000000,"sec")
    print('Nombre de mesures    : ',N_MEASURES)
    print("Fréquence effective  : ",N_MEASURES/((logDataT[N_MEASURES-1]-logDataT[0])/1000000000),"Hz ")


if __name__ == "__main__":
    main()
