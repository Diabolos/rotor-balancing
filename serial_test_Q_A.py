import serial
import binascii
import time
import struct

import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft, ifft

BAUDRATE = 115200
PORT = '/dev/ttyUSB0' # For windows 'COM7'

N_MEASURES = 30
FREQ = 0
N_SEND_SET_FREQ = 5
FREQUENCIES = {
  0: b'\x77\x05\x00\x0C\x00\x11',
  25: b'\x77\x05\x00\x0C\x04\x15',
  50: b'\x77\x05\x00\x0C\x05\x16',
  100: b'\x77\x05\x00\x0C\x06\x17',
  200: b'\x77\x05\x00\x0C\x07\x18',
  400: b'\x77\x05\x00\x0C\x08\x19',
}
HEADER_BLOCK = b'\x77\x10\x00\x6A'   # Chengtec ACC345 if scream mode enable cf. doc 2.6


ser = serial.Serial()

logDataT = np.empty((N_MEASURES))
logDataX = np.empty((N_MEASURES), dtype=np.float32)
logDataY = np.empty((N_MEASURES), dtype=np.float32)
logDataZ = np.empty((N_MEASURES), dtype=np.float32)
logDataR = np.empty((N_MEASURES), dtype=np.float32)
logDeltas = np.empty(N_MEASURES)

def initSerial():
    global ser
    ser.baudrate = BAUDRATE
    ser.port = PORT   
    ser.timeout = 10
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = 8
    ser.parity = serial.PARITY_NONE
    ser.rtscts = 0

def main():
    initSerial()
    global ser
    ser.open()
    for _ in range(N_SEND_SET_FREQ):
        ser.write(FREQUENCIES[FREQ])     
        
    ser.read_until(b'w')
    print("Freq = 0")
    print("réponse : ",ser.read(5))
    
    time.sleep(1)
    
    
    i=-1
    tStart = time.time_ns()

    while i<N_MEASURES-1:
        
        # Question
        ser.write(b'\x77\x05\x00\x2A\x00\x2F')  # Appel un block de 17 bytes avec acc sur 3 axes
     
        tReadStart = time.time_ns()
        ser.read_until(b'\x77\x10\x00\x6A',4)
              
        #mHex = ser.read(4)      # 77 10 00 6A : Standard block before x,y and z
        xHex = ser.read(4)      # 4 bytes for accX
        yHex = ser.read(4)      # 4 bytes for accY
        zHex = ser.read(4)      # 4 bytes for accZ
        mHex = ser.read(1)      # 1 byte for control, not used here
        print(xHex)

        i+=1
        logDataT[i] = time.time_ns()
        logDataX[i] = struct.unpack("<f", xHex)[0]
        logDataY[i] = struct.unpack("<f", yHex)[0]
        logDataZ[i] = struct.unpack("<f", zHex)[0]
  
        logDeltas[i] = time.time_ns()-tReadStart

        time.sleep(1)

    fig, ax = plt.subplots()
    ax.plot(logDataX)
    ax.plot(logDataY)
    ax.plot(logDataZ)
       
    plt.show()

    deltaPP = []
    for i in range(1,len(logDataT)):

        deltaPP.append(logDataT[i]-logDataT[i-1])
    
    print("mediane de deltaPP : ",np.median(deltaPP))
    print("mediane de deltaPP : ",np.average(deltaPP))
    print("mediane de logDeltas : ",np.median(logDeltas))
    print("interval idéal : ",1/FREQ)



if __name__ == "__main__":
    main()
