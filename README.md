# Rotor Balancing

Vibration analysis to perform rotor balancing

# Hardware

## Vibration meter : 

Chengtec ACC345

http://www.sh-chengtec.com


## Laser sensor :

OMCH HG-J12-D15P1

http://www.hugong.cn


# FFT Example

https://pythonnumericalmethods.berkeley.edu/notebooks/chapter24.04-FFT-in-Python.html

# Git upload new version of code

In terminal :

git add ./*

git commit -m "Mon Message de commit"

git push origin master

